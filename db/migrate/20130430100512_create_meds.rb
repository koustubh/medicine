class CreateMeds < ActiveRecord::Migration
  def change
    create_table :meds do |t|
      t.string :name
      t.text :order_code
      t.integer :quantity
      
      t.timestamps
    end
  end
end
