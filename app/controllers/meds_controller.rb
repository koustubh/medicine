class MedsController < ApplicationController
  # GET /meds
  # GET /meds.json
  def index
    @meds = Med.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @meds }
    end
  end

  def list
    @meds = Med.all
    render :list
  end
  
  # GET /meds/1
  # GET /meds/1.json
  def show
    @med = Med.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @med }
    end
  end

  # GET /meds/new
  # GET /meds/new.json
  def new
    @med = Med.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @med }
    end
  end

  # GET /meds/1/edit
  def edit
    @med = Med.find(params[:id])
  end

  # POST /meds
  # POST /meds.json
  def create
    @med = Med.new(params[:med])

    respond_to do |format|
      if @med.save
        format.html { redirect_to @med, notice: 'Med was successfully created.' }
        format.json { render json: @med, status: :created, location: @med }
      else
        format.html { render action: "new" }
        format.json { render json: @med.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /meds/1
  # PUT /meds/1.json
  def update
    @med = Med.find(params[:id])

    respond_to do |format|
      if @med.update_attributes(params[:med])
        format.html { redirect_to @med, notice: 'Med was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @med.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meds/1
  # DELETE /meds/1.json
  def destroy
    @med = Med.find(params[:id])
    @med.destroy

    respond_to do |format|
      format.html { redirect_to meds_url }
      format.json { head :no_content }
    end
  end
  
  def order
    @med = Med.find(params[:id])
    @output = @med.implement_order
    
    respond_to do |format|
      format.html { render :action => "order" }
      format.json { render json: output}
    end
  end
  
end
