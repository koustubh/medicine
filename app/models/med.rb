class Med < ActiveRecord::Base
  attr_accessible :name, :quantity, :order_code
  
  def implement_order
    code_string = self.order_code
    # symbolic_order_code =  order_code.to_sym
    # proc_order_code = symbolic_order_code.to_proc
    # output = proc_order_code.call
    output = eval("begin $stdout = StringIO.new; #{code_string}; $stdout.string;
ensure $stdout = STDOUT end")
    return output
  end
  
end
